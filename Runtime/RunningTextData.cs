﻿
namespace Gtion.Plugin.RunningText
{
    [System.Serializable]
    public class RunningTextData
    {
        public string Text;
        public float Speed;
    }
}
