﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using TMPro;

namespace Gtion.Plugin.RunningText
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class RunningText_TMProUGUI : MonoBehaviour
    {
        [SerializeField]
        public TextMeshProUGUI textMeshPro;

        private bool isSkip;
        private bool listSkip;
        private string baseValue = string.Empty;


        protected void Start()
        {
            textMeshPro.text = string.Empty;
            textMeshPro = GetComponent<TextMeshProUGUI>();
        }

        public void Animate(string text, float speed, Action onFinished = null)
        {
            StartCoroutine(TypeLine(text, speed, onFinished));
        }

        public void Animate(RunningTextData dataModel, Action onFinished = null)
        {
            Animate(dataModel.Text,dataModel.Speed,onFinished);
        }

        public void Animate(List<RunningTextData> dataModels, Action onFinished = null)
        {
            StartCoroutine(TypelineList(dataModels, onFinished));
        }
		
		public void SkipAnimation()
        {
            isSkip = true;
            listSkip = true;
        }
        IEnumerator TypelineList(List<RunningTextData> dataModels , Action onFinished)
        {
            baseValue = string.Empty;
            bool isLoading = false;
            foreach(RunningTextData model in dataModels)
            {
                isSkip = listSkip;
                Debug.Log(model.Text);
                Animate(model, () => isLoading = true);
                yield return new WaitUntil(() => isLoading);
                isLoading = false;
                baseValue += model.Text;
            }
            listSkip = false;
            onFinished?.Invoke();
        }

        IEnumerator TypeLine(string text , float speed , Action onFinished)
        {
            DateTime call = DateTime.Now;
            int i = 0;
            WaitForEndOfFrame delay = new WaitForEndOfFrame();
            while (i < text.Length - 1 &&(!isSkip))
            {
                TimeSpan totalDeltaTime = DateTime.Now - call;
                float Second = (float)totalDeltaTime.TotalSeconds;
                i = Mathf.Min(text.Length - 1, Mathf.FloorToInt(speed * Second));
                textMeshPro.text = baseValue + text.Substring(0, i);
                yield return delay;
            }
            textMeshPro.text = baseValue + text;
            isSkip = false;
            onFinished?.Invoke();
        }
    }
}