# Gtion Running Text
## Built In
![Build Status](https://badgen.net/badge/Build/Passing/green) ![Version](https://badgen.net/badge/Release/v1.1.0/blue) ![Languages](https://badgen.net/badge/Languages/C-Sharp/blue)
![Engine](https://badgen.net/badge/Engine/Unity%202020.3.4f1/gray)

Depedencies
- TMPro

## Installation
Please check documentation below
> https://docs.unity3d.com/2019.3/Documentation/Manual/upm-ui-giturl.html

## What is it?
Animation Tools 

## Contributor


| Profile | 
| ------ |
| [![Firdiar](https://gitlab.com/uploads/-/system/user/avatar/2307294/avatar.png?raw=true)](https://www.linkedin.com/in/firdiar) |
| [Firdiansyah Ramadhan](https://www.linkedin.com/in/firdiar) | 
|![Xenosians](https://gitlab.com/uploads/-/system/user/avatar/9070212/avatar.png?width=400)|
|[Alifio Yudhistira Aji Salis](https://www.linkedin.com/in/alifio-yudhistira-aji-salis-0170721b9/)|


## License

MIT

** My life is a constant battle between my love of food and not wanting to get fat. **
